# Nuget Dependency Resolution Tests #

Questo progetto ha lo scopo di chiarire come si comporta il package manager di nuget.

### What is this repository for? ###

* 1.0.0
* [https://nuget.codeplex.com/wikipage?title=Dependency%20Resolution](https://nuget.codeplex.com/wikipage?title=Dependency%20Resolution)
* [dependencies] (https://anvaka.github.io/pm/?imm_mid=0d9c03&cmp=em-prog-na-na-newsltr_20151003#/galaxy/nuget?cx=402&cy=-1762&cz=-617&lx=0.0509&ly=-0.9433&lz=0.2796&lw=0.1717&ml=150&s=1.75&l=1&v=2015-08-01T10-00-00Z)

### Contents ###

* Dependency Upgrade Scenario

### Dependency Upgrade Scenario ###

**Installed**

RavenDB –> Newtonsoft.Json (2.0.0)
Newtonsoft.Json 1.0.0

**Installing:** Elmah –> log4net [2.0.0, 4.0.0)

**Available:** log4net v1.0, 1.1, 2.0.0, 2.0.5, 2.1.0, 3.0, 3.2.0, 3.6.0, 4.0.0

**Result:** Log4Net upgraded to v2.0.5